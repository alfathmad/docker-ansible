FROM ubuntu:22.04

# Install dependencies and Ansible
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    apt-add-repository --yes --update ppa:ansible/ansible && \
    apt-get install -y ansible

# Set the working directory
WORKDIR /ansible